<?php
#関数・定数の読み込み
require_once("function.php");

#データベース
$pdo = connect_db();
$st = $pdo->query("SELECT * FROM `post` ORDER BY id DESC");
$posts = $st->fetchAll();

#コメントをpost配列に挿入
for($i = 0; $i < count($posts); $i++){
	$id = $posts[$i]["id"];
	$st =$pdo->query("SELECT * FROM `comment` WHERE `post_id` = $id ORDER BY id DESC");
	$comments = $st->fetchAll();
	$posts[$i]["comments"] = $comments;
}

#テンプレートの読み込み
include("tmpl/header.tmpl");
include("tmpl/top.tmpl");
include("tmpl/footer.tmpl");

function delete_db($id){
    $st =$pdo->query("DELETE FROM `post` WHERE `id` = $id");
}

?>