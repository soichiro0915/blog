<?php
#定数・関数・クラスの読み込み
require_once("function.php");
$vali = new Validation();

$error = "";
$name = "";
$content = "";

if(isset($_GET["post_id"])){
$post_id =  intval($_GET["post_id"]);
}else {
	header("Location: index.php");
	exit();
}



if(isset($_POST["submit"])){
	$post_id = $_POST["post_id"];
    $name = $_POST["name"];
    $content = $_POST["content"];

    $vali->checkEmpty($name, "名前");
	$vali->checkMaxLength($name, "名前", 100);
    $vali->checkEmpty($content, "コメント");
    $vali->checkMinLength($content, "コメント", 5);

    $error = $vali->get_error();

	if(!$error){
    $pdo = connect_db();
    $st = $pdo->prepare("INSERT INTO `comment` ( `post_id`, `name`, `content`) VALUES (?, ?, ?)");
	$st->execute(array($post_id, $name, $content));

	#リダイレクト
	header("Location: index.php");
	exit();
	}
}


include("tmpl/header.tmpl");
include("tmpl/comment.tmpl");
include("tmpl/footer.tmpl");

?>

		
