<?php
const DB_NAME = "blog";
const DB_ACCOUNT = "root";

function connect_db(){
    $pdo = new PDO("mysql:dbname=".DB_NAME, DB_ACCOUNT, "");
    return $pdo;
}

function v($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
}

function datetime_format($time){
    $date = new DateTime($time);
	return $date->format("Y年m日d日　H:i");
}

class Validation{
    #エラーを代入するプロパティ
    #public $error = "";
    private $error = "";

    #入力されているかのメソッド
    #checkEmpty()
    public function checkEmpty($text, $formname){
        if(!$text) $this->error .= $formname."を入力してください。<br>";
	
    }

    #長すぎないかチェックするメソッド
    #checkMaxLength()
    public function checkMaxLength($text, $formname, $max_num) {
		if(strlen($text) > $max_num) $this->error .= $formname."が長すぎます。".$max_num."文字以下でお願いします。<br>";
	}

    #短すぎないかどうかチェックするメソッド
    #checMinLength()
    public function checkMinLength($text, $formname, $min_num) {
		if($text && strlen($text) < $min_num) $this->error .= $formname."が短すぎます。".$min_num."文字以上でお願いします。<br>";
	}

    #メールアドレスかどうかのメソッド
    #checkEmailAddress()
    public function checkEmailAddress($email){
        if (!preg_match("/\w+@\w+/",$email)) $this->error .= "メールアドレスが正しくありません<br>";
    }

    #エラーがあるかどうかのメソッド
    #getError()
    public function get_error() {
        return $this->error;
    }
}