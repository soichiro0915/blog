<?php
#定数・関数・クラスの読み込み
require_once("function.php");

$error = "";
$title = "";
$content = "";

if(isset($_POST["submit"])) {

	$title = $_POST["title"];
	$content = $_POST["content"];


    $vali = new Validation();
    $vali->checkEmpty($title, "タイトル");
    $vali->checkMaxLength($title, "タイトル", 50);
    $vali->checkMinLength($title, "タイトル", 3);
    $vali->checkEmpty($content, "本文");
    $vali->checkMinLength($content, "本文", 5);

    $error = $vali->get_error();

    if(!$error){
        #データベースにタイトルとコンテンツを挿入
	    $pdo = connect_db();
	    $st = $pdo->prepare("INSERT INTO `post` (`title`, `content`) VALUES (?, ?)");
        $st->execute(array($title, $content));

        #写真をimgsファイルに保存
        $id = $pdo->lastInsertId(); #挿入したばかりのデータのIDを取得
        if($_FILES["image"]["size"] > 0):
            move_uploaded_file($_FILES["image"]["tmp_name"], "imgs/upload_{$id}.jpg");
        endif;

        #リダイレクト
        header("Location: index.php");
        exit();
    }

}

include("tmpl/header.tmpl");
include("tmpl/post.tmpl");
include("tmpl/footer.tmpl");



?>


